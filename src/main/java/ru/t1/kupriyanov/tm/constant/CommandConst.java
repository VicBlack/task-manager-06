package ru.t1.kupriyanov.tm.constant;

public class CommandConst {

    public static final String ABOUT = "about";

    public static final String VERSION = "version";

    public static final String HELP = "help";

    public static final String EXIT = "exit";

}
