package ru.t1.kupriyanov.tm;

import ru.t1.kupriyanov.tm.constant.ArgumentConst;
import ru.t1.kupriyanov.tm.constant.CommandConst;

import java.util.Scanner;

import static ru.t1.kupriyanov.tm.constant.CommandConst.*;

public class Application {

    public static void main(String[] args) {
        processArguments(args);
        System.out.println("** WELCOME TASK MANAGER **");
        final Scanner scanner = new Scanner(System.in);
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND: ");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    private static void processArguments(String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
        System.exit(0);
    }

    private static void processArgument(String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.HELP:
                showHelp();
                break;
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            default:
                showArgumentError();
                break;
        }
    }

    private static void processCommand(String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case CommandConst.HELP:
                showHelp();
                break;
            case CommandConst.ABOUT:
                showAbout();
                break;
            case CommandConst.VERSION:
                showVersion();
                break;
            case CommandConst.EXIT:
                exit();
                break;
            default:
                showCommandError();
                break;
        }
    }

    private static void exit() {
        System.exit(0);
    }

    private static void showArgumentError() {
        System.err.println("[ERROR]");
        System.err.println("This argument is not supported.");
        System.exit(1);
    }

    private static void showCommandError() {
        System.err.println("[ERROR]");
        System.err.println("This argument is not supported.");
        System.exit(1);
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s, %s: Show list arguments.\n", CommandConst.HELP, ArgumentConst.HELP);
        System.out.printf("%s, %s: Show info about programmer.\n", CommandConst.ABOUT, ArgumentConst.ABOUT);
        System.out.printf("%s, %s: Show version.\n", CommandConst.VERSION, ArgumentConst.VERSION);
        System.out.printf("%s: Close application.\n", EXIT);
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Anton Kupriyanov");
        System.out.println("E-mail: ankupr29@gmail.com");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.6.0");
    }

}
